<div data-anchor="page4" class="pp-scrollable section section-4">
  <div class="scroll-wrap">
    <div class="section-bg bg-about" style="background-image:url(images/bg/about.jpg);"></div>
    <div class="scrollable-content">
      <div class="vertical-title text-white  d-none d-lg-block"><span>about us</span></div>
      <div class="vertical-centred">
        <div class="boxed boxed-inner">
          <div class="boxed">
            <div class="container">
              <div class="intro">
                <div class="row align-items-center">
                  <div class="col-lg-6 offset-xl-1">
                    <div class="experience-box">
                      <div class="experience-content">
                        <div class="experience-number">1</div>
                        <div class="experience-info">
                          <div>Years<br>Experience<br>Working</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-5 mt-5 mt-xl-0">
                    <h2 class="title"> <span class="text-primary">Standard</span>&nbsp; Code</h2>

                    <div class="progress-bars">
                      <div class="clearfix">
                        <div class="number float-left">Web Design</div>
                        <div class="number float-right">80%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar-wrp">
                          <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                      <div class="clearfix">
                        <div class="number float-left">Web Development</div>
                        <div class="number float-right">70%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar-wrp">
                          <div class="progress-bar" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                      <div class="clearfix">
                        <div class="number float-left">Java Fxml</div>
                        <div class="number float-right">90%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar-wrp">
                          <div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>

                      <div class="clearfix">
                        <div class="number float-left">MySQL</div>
                        <div class="number float-right">90%</div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar-wrp">
                          <div class="progress-bar" role="progressbar" style="width: 90%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>