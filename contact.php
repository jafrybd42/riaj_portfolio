<div data-anchor="page8" class="pp-scrollable section section-8">
  <div class="scroll-wrap">
    <div class="section-bg" style="background-image:url(image/map.jpg);"></div>
    <div class="scrollable-content">
      <div class="vertical-title text-white d-none d-lg-block"><span>contact</span></div>
      <div class="vertical-centred">
        <div class="boxed boxed-inner">
          <div class="boxed">
            <div class="container">
              <div class="intro overflow-hidden">
                <div class="row">
                  <div class="col-md-6">
                    <h2 class="title"><span class="text-primary">Dhaka</span>, Bangladesh</h2>
                    <!--  <h5 class="text-muted">Adress in details</h5> -->
                    <section class="contact-address">
                      <h3><a class="mail" href="mailto:riaz.islam4542@gmail.com"> <span class="__cf_email__">Contact in Email</span></a></h3>
                      <h3><a class="mail" href="tel:+8801672940784"><span class="phone">+88 016 7294 0784</span></a></h3>
                    </section>
                  </div>
                  <div class="col-md-6">
                    <div class="contact-info">
                      <form action="mail.php" class="js-form" novalidate="novalidate">
                        <div class="row">
                          <div class="form-group col-sm-6">
                            <input type="text" name="name" required="" placeholder="Name*" aria-required="true">
                          </div>
                          <div class="form-group col-sm-6">
                            <input type="email" required="" name="email" placeholder="Email*">
                          </div>
                          <div class="form-group col-sm-12">
                            <input type="text" name="subject" placeholder="Subject (Optinal)">
                          </div>
                          <div class="form-group col-sm-12">
                            <textarea name="message" required="" placeholder="Message*"></textarea>
                          </div>
                          <div class="form-group form-group-message col-sm-12">
                            <span id="success" class="text-primary">Thank You, your message is successfully sent!</span>
                            <span id="error" class="text-primary">Sorry, something went wrong </span>
                          </div>
                          <div class="col-sm-12"><button type="submit" class="btn">Contact me</button></div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>