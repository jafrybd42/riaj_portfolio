<div data-anchor="page3" class="pp-scrollable text-white section section-3">
  <div class="scroll-wrap">
    <div class="section-bg mask" style="background-image:url(images/bg/resume.jpg);"></div>
    <div class="scrollable-content">
      <div class="vertical-title d-none d-lg-block"><span>resume</span></div>
      <div class="vertical-centred">
        <div class="boxed boxed-inner">
          <div class="boxed">
            <div class="container">
              <div class="intro">
                <div class="row row-resume">
                  <div class="col-md-6 fadeY fadeY-1">
                    <h2 class="resume-header title">Education</h2>
                    <div class="col-resume">
                      <div class="resume-content">
                        <div class="resume-inner">
                          <div class="resume-row">
                            <h6 class="resume-type">Master’s in Business Administration (MBA)</h6>
                            <i class="resume-study">Brac University<br>Running - 2021</i>
                          </div>
                          <div class="resume-row">
                            <h6 class="resume-type">BSC in Computer Science and Engineering</h6>
                            <i class="resume-study">Southeast University<br>Passing Year - 2020</i>
                            <p class="resume-text">CGPA : &nbsp; 2.96</p>
                          </div>
                          <div class="resume-row">
                            <h6 class="resume-type">Higher Secondary School Certificate</h6>
                            <i class="resume-study">Govt. Science College<br>Batch - 2014</i>
                            <p class="resume-text">GPA : &nbsp;4.90</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 pt-md-5 mt-md-5 fadeY fadeY-2">
                    <h2 class="resume-header title">Experience</h2>
                    <div class="col-resume">
                      <div class="resume-content">
                        <div class="resume-inner">
                          <div class="resume-row">
                            <h6 class="resume-type">Data Analyst</h6>
                            <i class="resume-study">SEBPO<br>December 2020 - Present</i>
                          </div>
                          <div class="resume-row">
                            <h6 class="resume-type">Student Ambassador</h6>
                            <i class="resume-study">Southeast University<br>Jan 2019 - Aug 2019</i>
                          </div>
                          <div class="resume-row">
                            <h6 class="resume-type">Volunteer</h6>
                            <i class="resume-study">ICPC Dhaka Regional<br>December, 2019</i>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>